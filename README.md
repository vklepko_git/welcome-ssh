# Приветствие SSH на Ubuntu и CentOS

## Установка на сервер

Заходим на сервер и переходим под пользователя **root**.

Устанавливаем Git:

```sh
apt update
apt install -y git
```

Чтобы отключить default приветствие нужно отключить motd в конфигурации демона SSH. Установите **PrintMotd** в **/etc/ssh/sshd_config** в **no** и перезапустите демон ssh.

```sh
vim /etc/ssh/sshd_config 
#Ubuntu
systemctl restart ssh.service
#CentOS
systemctl restart sshd.service
```

Очищаем файл:

```sh
cat /dev/null > /etc/motd
```

Cоздаем пустой файл (~/.hushlogin) в вашей домашней папке. Демоны, которые разрешают удаленные входы в систему, традиционно проверяют наличие этого файла, и если он присутствует, они позволят вам войти в систему спокойно.

```sh
touch /root/.hushlogin
```

Теперь скачиваем репозиторий со скриптом:

```sh
git clone https://gitlab.com/vklepko_git/welcome-ssh.git
```
Переходим в скачанный каталог и переносим файл в /etc/profile.d/ : 

```sh
cd welcome-ssh
mv welcome_ssh.sh /etc/profile.d/
```

Даем права на выполнение:

```sh
chmod +x /etc/profile.d/welcome_ssh.sh
```
