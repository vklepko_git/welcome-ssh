#!/bin/bash
 
# clear the screen
clear
 
unset tecreset os architecture kernelrelease internalip externalip nameserver dnsservers loadaverage1 loadaverage5 loadaverage15
 
echo " " 
 
while getopts iv name
do
        case $name in
          i)iopt=1;;
          v)vopt=1;;
          *)echo "Invalid arg";;
        esac
done
 
if [[ ! -z $iopt ]]
then
{
wd=$(pwd)
basename "$(test -L "$0" && readlink "$0" || echo "$0")" > /tmp/scriptname
scriptname=$(echo -e -n $wd/ && cat /tmp/scriptname)
su -c "cp $scriptname /usr/bin/monitor" root && echo "Congratulations! Script Installed, now run monitor Command" || echo "Installation failed"
}
fi
 
if [[ ! -z $vopt ]]
then
{
echo -e "tecmint_monitor version 0.1\nDesigned by Tecmint.com\nReleased Under Apache 2.0 License"
}
fi
 
if [[ $# -eq 0 ]]
then
{
 
 
# Define Variable tecreset
tecreset=$(tput sgr0)
 
# Check if connected to Internet or not
ping -c 1 google.com &> /dev/null && echo -e '\E[32m'"Internet: $tecreset Connected" || echo -e '\E[32m'"Internet: $tecreset Disconnected"
 
# Check OS Type
os=$(uname -o)
echo -e '\E[32m'"Operating System Type :" $tecreset $os
 
# Check OS Release Version and Name
cat /etc/os-release | grep 'NAME\|VERSION' | grep -v 'VERSION_ID' | grep -v 'PRETTY_NAME' | grep -v 'UBUNTU_CODENAME\|CPE_NAME\|CENTOS_MANTISBT_PROJECT_VERSION\|REDHAT_SUPPORT_PRODUCT_VERSION' > /tmp/osrelease
echo -n -e '\E[32m'"OS Name :" $tecreset  && cat /tmp/osrelease | grep -v "VERSION" | cut -f2 -d\"
echo -n -e '\E[32m'"OS Version :" $tecreset && cat /tmp/osrelease | grep -v "NAME" | cut -f2 -d\"
 
# Check Architecture
architecture=$(uname -m)
echo -e '\E[32m'"Architecture :" $tecreset $architecture
 
# Check Kernel Release
kernelrelease=$(uname -r)
echo -e '\E[32m'"Kernel Release :" $tecreset $kernelrelease
 
# Check hostname
echo -e '\E[32m'"Hostname :" $tecreset $HOSTNAME
 
# Check Internal IP
internalip=$(hostname -I)
echo -e '\E[32m'"Internal IP :" $tecreset $internalip
 
# Check External IP
externalip=$(curl -s 2ip.ua | grep ip | awk '{print $3}')
echo -e '\E[32m'"External IP : $tecreset "$externalip
 
# Check Name and DNS Servers
nameservers=$(cat /etc/resolv.conf | grep "nameserver" | awk '{print $2}')
dnsservers=$(cat /etc/resolv.conf | grep  "search" | grep -v "#" | sed 's/search //g')
echo -e '\E[32m'"Name Servers :" $tecreset $nameservers 
echo -e '\E[32m'"DNS Servers :" $tecreset $dnsservers
# Check Logged In Users
who>/tmp/who
echo -e '\E[32m'"Logged In users :" $tecreset && cat /tmp/who 
 
# Check RAM and SWAP Usages
free -h | grep -v + > /tmp/ramcache
echo -e '\E[32m'"Ram/Swap Usages :" $tecreset
cat /tmp/ramcache
 
# Check Disk Usages
df -h| grep 'Filesystem\|/dev/*' > /tmp/diskusage
echo -e '\E[32m'"Disk Usages :" $tecreset 
cat /tmp/diskusage
 
# Check Load Average
loadaverage1=$(uptime | awk '{print $9}') 
loadaverage5=$(uptime | awk '{print $10}')
loadaverage15=$(uptime | awk '{print $11}')
echo -e '\E[32m'"Load Average :" $tecreset  "1 min - "$loadaverage1 "| 5 min - "$loadaverage5 "| 15 min - "$loadaverage15
 
# Check System Uptime
tecuptime=$(uptime | awk '{print $3,$4}' | cut -f1 -d,)
echo -e '\E[32m'"System Uptime Days/(HH:MM) :" $tecreset $tecuptime
 
# Unset Variables
unset tecreset os architecture kernelrelease internalip externalip nameserver dnsservers loadaverage1 loadaverage5 loadaverage15
 
# Remove Temporary Files
rm /tmp/osrelease /tmp/who /tmp/ramcache /tmp/diskusage
}
fi
shift $(($OPTIND -1))
echo " " 
